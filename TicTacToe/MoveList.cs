﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace TicTacToe
{
    /*
     * класс MoveList представляет собой лист решений
    */
    public class MoveList : IEnumerable
    {
        public List<Move> List;
        public int Potential;   //--потенциал комбинации 1  - самый потенциальный. 100 - самый НЕ потенциальный


        public MoveList()
        {
            List = new List<Move>();
        }

        //--возвращает символ по координатам
        public int GetSymbol(int x, int y)
        {
            return this.List.Count == 0 ? 0 : (from c in this.List where c.X == x && c.Y == y select c.Symbol).FirstOrDefault();
        }
        //--добавления хода в лист ходов
        public void Add(int x, int y, int symbol)
        {
            if (symbol >= 0 && symbol < 3)
                List.Add(new Move(x, y, symbol));
            else
                throw new Exception("Ход невозможен. Неверный Сивол!");
        }
        public void Add(Move move)
        {
            if (move.Symbol >= 0 && move.Symbol < 3)
                List.Add(move);
            else
                throw new Exception("Ход невозможен. Неверный Сивол!");

        }
        //--очистка листа ходов
        public void Clear()
        {
            List.Clear();
        }
        //--возвращает кол-во элементов
        public int Count()
        {
            return this.List.Count;
        }
        //  --  АНАЛИЗ  --
        //  -- считает количество символов в комбинации
        public int SymbCount(int Cymb)
        {
            return this.List.Count(v => v.Symbol == Cymb);
   
        }

        public IEnumerator GetEnumerator()
        {
            return ((IEnumerable)List).GetEnumerator();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    public class Move
    {

        public int X;
        public int Y;
        public int Symbol;
        /*
            * 0 - клетка пуста
            * 1 - крестик
            * 2 - нолик
        */
        public Move(int x, int y, int s)
        {
            this.X = x;
            this.Y = y;
            this.Symbol = s;
        }

    }
}

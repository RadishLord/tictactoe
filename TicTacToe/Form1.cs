﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace TicTacToe
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        #region declare

        private Thread AvAThread;                   //--поток для режима ai vs ai
        private MoveList _moveList;

        /*
           * 0 - клетка пуста
           * 1 - крестик
           * 2 - нолик
       */
        private TicTacToeAi _ai = new TicTacToeAi(); //--бот
        private Bitmap _mb;                          //--графика
        private Graphics _g;                         //--графика
        private int _i0 = 0;                         //--начало отрисвки поля по горизонтали
        private int _j0 = 0;                         //--начало отрисвки поля по вертикали
        int _step = 1;                               //--флаг активного игрока
                                                     /*
                                                         * 0 - никто не ходит
                                                         * 1 - ход первого игрока
                                                         * 2 - ход второго игрока
                                                     */
        #endregion

        #region события формы
        //--загрузка формы
        private void Form1_Load(object sender, EventArgs e)
        {

            _moveList = new MoveList();
            PaintField();   //--отрисовка поля
            statusLabel.Text = $"Ход игрока №{_step}";
            _ai.Log = rtbLog;

            AvAThread = new Thread(delegate ()  {})
            { IsBackground = true };
        }
        //--клик по полю
        private void pictureBox_Click_1(object sender, EventArgs e)
        {
            try
            {
                Point p = pictureBox.PointToClient(System.Windows.Forms.Cursor.Position);
                int i = (p.X / 20) - _i0;
                int j = (p.Y / 20) - _j0;

                if (_moveList.GetSymbol(i, j) != 0)
                    throw new Exception("Клетка занята!");

                if (rbPvP.Checked)
                    PvPMove(i, j);
                if (rbPvA.Checked  )
                    PvAMove(i, j);
                if (rbAvP.Checked)
                    AvPMove(i,j);

            }
            catch (Exception ex)
            {
                MessageBox.Show($"{ex.Message}");
                statusLabel.Text = $"{ex.Message}";
            }

        }
        //--показывает координаты
        private void pictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            Point p = pictureBox.PointToClient(System.Windows.Forms.Cursor.Position);
            label0.Text = $"{p.X / 20 - _i0} - {p.Y / 20 - _j0}";
        }
        //--кнопка "новая игра"
        private void btNewGame_Click(object sender, EventArgs e)
        {
            NewGame();
            if (rbAvP.Checked)
                FirstAImove();
        }
        //--
        private void Form1_Resize(object sender, EventArgs e)
        {
            PaintField();
            FillField();
        }
        #endregion

        #region game controller
        //--обработка хода в режиме игрок против игрока
        private void PvPMove(int i, int j)
        {

            _moveList.Add(new Move(i, j, _step));
            FillField();

            VictoryCheck(i, j, _step);  //--проверка на победу

            //--смена хода
            switch (_step)
            {
                case 1:
                    _step = 2;
                    break;
                case 2:
                    _step = 1;
                    break;
                default:
                    return;
            }
            statusLabel.Text = $"Ход игрока №{_step}";
        }
        //--обработка хода в режиме ai против ai
        private void AvAMove()
        {
            AvAThread = new Thread(delegate ()
            {
                int i = 0, j = 0;
                do
                {
                    var i1 = _step == 1 ? _step = 2 : _step = 1;
                    statusLabel.Text = $"Ход компьютера №{_step}";
                    _ai.Move(_moveList, ref i, ref j, _step);
                    _moveList.Add(i, j, _step);
                    Action updateImgAction = FillField;
                    Invoke(updateImgAction);
                    Thread.Sleep(200);

                } while (!isWin(i, j, _step));

                MessageBox.Show($"Выиграл бот №{_step}");
                statusLabel.Text = $"Выиграл бот №{_step}";

                bAvAStop.Invoke((MethodInvoker)delegate ()
                {
                    bAvAStop.Visible = false;
                    btNewGame.Visible = true;
                });
            })
            { IsBackground = true };

            AvAThread.Start();
        }
        //--обработка хода в режиме игрок против ai
        private void PvAMove(int i, int j)
        {
            if (_step == 0) return;
            _moveList.Add(i, j, 1);
            FillField();

            VictoryCheck(i, j, 1);          //--проверка на победу 
            if (_step == 0) return;
            //--ход AI
            statusLabel.Text = $"Ход компьютера";
            _ai.Move(_moveList, ref i, ref j, 2);
            _moveList.Add(i, j, 2);
            FillField();

            statusLabel.Text = $"Ход игрока";

            VictoryCheck(i, j, 2);  //--проверка на победу для AI            
        }
        //--обработка хода в режиме ai против игрока
        private void AvPMove(int i, int j)
        {
            if (_step == 0) return;
            _moveList.Add(i, j, 2);
            FillField();

            VictoryCheck(i, j, 2);          //--проверка на победу 
            if (_step == 0) return;
            //--ход AI
            statusLabel.Text = $"Ход компьютера";
            _ai.Move(_moveList, ref i, ref j, 1);
            _moveList.Add(i, j, 1);
            FillField();
            statusLabel.Text = $"Ход игрока";

            VictoryCheck(i, j, 1);  //--проверка на победу для AI
            //--смена хода

            
        }
        //--первый ход компьютера
        private void FirstAImove()
        {
            int i = 0, j = 0;
            statusLabel.Text = $"Ход компьютера";
            _ai.Move(_moveList, ref i, ref j, 2);
            _moveList.Add(i, j, 1);
            FillField();
        }
        //--отрисовка поля
        private void PaintField()
        {
            if (this.WindowState == System.Windows.Forms.FormWindowState.Minimized)
            {
                return;
            }
            _mb = new Bitmap(pictureBox.Width, pictureBox.Height);
            Pen p = new Pen(Color.Black, 1);
            _g = Graphics.FromImage(_mb);
            Action updateImgAction = () => { pictureBox.Image = _mb; };
            pictureBox.Image = _mb;
            float w = pictureBox.Width;
            float h = pictureBox.Height;
            _g.FillRectangle(Brushes.White, 0, 0, w, h);
            for (int i = 0; i < h; i += 20)
            {
                _g.DrawLine(p, 0, i, w, i);
            }
            for (int i = 0; i < w; i += 20)
            {
                _g.DrawLine(p, i, 0, i, h);
            }
            Invoke(updateImgAction);
        }
        //--заполнение поля
        private void FillField()
        {

            PaintField();
            foreach (Move c in _moveList.List)
            {
                SetSymbol(c.X + _i0, c.Y + _j0, c.Symbol);
            }
            if (_moveList.Count() > 0)
            {
                Move last = _moveList.List[_moveList.List.Count - 1];
                TouchUpSymbol(last.X + _i0, last.Y + _j0, last.Symbol);
            }

        }
        //--установка символа х или о
        public void SetSymbol(int x, int y, int z)
        {
            int w = pictureBox.Width;
            int h = pictureBox.Height;
            x = x * 20;
            y = y * 20;


            switch (z)
            {
                case 0:
                    {
                        SolidBrush br = new SolidBrush(Color.White);
                        _g.FillRectangle(br, x + 1, y + 1, 19, 19);
                    }
                    break;
                case 1: //--отрисовка крестика
                    {
                        Pen p = new Pen(Color.Black, 2);
                        _g.DrawLine(p, x + 1, y + 1, x + 19, y + 19);
                        _g.DrawLine(p, x + 19, y + 1, x + 1, y + 19);
                    }
                    break;
                case 2: //--отрисовка нолика
                    {
                        Pen p = new Pen(Color.Red, 2);
                        _g.DrawEllipse(p, x + 1, y + 1, 19, 19);
                    }
                    break;
            }


            _g = Graphics.FromImage(_mb);
            pictureBox.Image = _mb;
            Action updateImgAction = () => { pictureBox.Image = _mb; };
            Invoke(updateImgAction);
        }
        //--выделяет последний символ
        public void TouchUpSymbol(int x, int y, int z)
        {
            int w = pictureBox.Width;
            int h = pictureBox.Height;
            x = x * 20;
            y = y * 20;

            SolidBrush br = new SolidBrush(Color.Yellow);
            _g.FillRectangle(br, x + 1, y + 1, 19, 19);
            switch (z)
            {
                case 1: //--отрисовка крестика
                    {
                        Pen p = new Pen(Color.Black, 2);
                        _g.DrawLine(p, x + 1, y + 1, x + 19, y + 19);
                        _g.DrawLine(p, x + 19, y + 1, x + 1, y + 19);
                    }
                    break;
                case 2: //--отрисовка нолика
                    {
                        Pen p = new Pen(Color.Red, 2);
                        _g.DrawEllipse(p, x + 1, y + 1, 19, 19);
                    }
                    break;
            }


            _g = Graphics.FromImage(_mb);
            pictureBox.Image = _mb;
            Action updateImgAction = () => { pictureBox.Image = _mb; };
            Invoke(updateImgAction);
        }
        //--событие конца игры
        private void EndGame()
        {
            btNewGame.Visible = true;
        }
        //--событие начала игры
        private void NewGame()
        {
            btNewGame.Visible = false;
            _moveList.Clear();
            if (rbAvA.Checked)
            {
                bAvAStop.Visible = true;
                AvAMove();
                return;
            }
            _step = 1;
            _moveList.Clear();
            PaintField();
            statusLabel.Text = $"Ход игрока №{_step}";
        }
        #endregion

        #region проверка победы
        private void VictoryCheck(int x, int y, int player)
        {

            bool victory = (CheckDiagonalA(x, y, player) || CheckDiagonalB(x, y, player)
                || CheckLineA(x, y, player) || CheckLineB(x, y, player));


            if (victory)
            {
                if (rbPvA.Checked)
                {
                    if (player == 1)
                    {
                        statusLabel.Text = $"Игрок победил!";
                        MessageBox.Show($"Игрок победил!");
                    }
                    else if (player == 2)
                    {
                        statusLabel.Text = $"Компьютер победил!";
                        MessageBox.Show($"Компьютер победил!");
                    }
                }
                else if (rbAvP.Checked)
                {
                    if (player == 2)
                    {
                        statusLabel.Text = $"Игрок победил!";
                        MessageBox.Show($"Игрок победил!");
                    }
                    else if (player == 1)
                    {
                        statusLabel.Text = $"Компьютер победил!";
                        MessageBox.Show($"Компьютер победил!");
                    }
                }
                else if (rbPvP.Checked)
                {
                    statusLabel.Text = $"Игрок {player} победил!";
                    MessageBox.Show($"Игрок {player} победил!");
                }
                EndGame();
                _step = 0;
            }
            else
            {
                return;
            }
        }
        private bool isWin(int x, int y, int player)
        {

            return (CheckDiagonalA(x, y, player) || CheckDiagonalB(x, y, player)
                || CheckLineA(x, y, player) || CheckLineB(x, y, player));


        }
        bool CheckDiagonalA(int x, int y, int player)
        {
            bool right = true, left = true;
            int rCount = 1, lCount = 1;
            int xx = x, yy = y;
            while (right && rCount < 5)
            {
                if (_moveList.GetSymbol(++xx, ++yy) == player)
                    rCount++;
                else
                    right = false;
            }
            while (left && lCount < 5)
            {
                if (_moveList.GetSymbol(--x, --y) == player)
                    lCount++;
                else
                    left = false;
            }
            if (rCount + lCount > 5)
                return true;
            else
                return false;
        }
        bool CheckDiagonalB(int x, int y, int player)
        {
            bool right = true, left = true;
            int rCount = 1, lCount = 1;
            int xx = x, yy = y;
            while (right && rCount < 5)
            {
                if (_moveList.GetSymbol(++xx, --yy) == player)
                    rCount++;
                else
                    right = false;
            }
            while (left && lCount < 5)
            {
                if (_moveList.GetSymbol(--x, ++y) == player)
                    lCount++;
                else
                    left = false;
            }
            if (rCount + lCount > 5)
                return true;
            else
                return false;
        }
        bool CheckLineA(int x, int y, int player)
        {
            bool right = true, left = true;
            int rCount = 1, lCount = 1;
            int xx = x, yy = y;
            while (right && rCount < 5)
            {
                if (_moveList.GetSymbol(xx, --yy) == player)
                    rCount++;
                else
                    right = false;
            }
            while (left && lCount < 5)
            {
                if (_moveList.GetSymbol(x, ++y) == player)
                    lCount++;
                else
                    left = false;
            }
            if (rCount + lCount > 5)
                return true;
            else
                return false;
        }
        bool CheckLineB(int x, int y, int player)
        {
            bool right = true, left = true;
            int rCount = 1, lCount = 1;
            int xx = x, yy = y;
            while (right && rCount < 5)
            {
                if (_moveList.GetSymbol(--xx, yy) == player)
                    rCount++;
                else
                    right = false;
            }
            while (left && lCount < 5)
            {
                if (_moveList.GetSymbol(++x, y) == player)
                    lCount++;
                else
                    left = false;
            }
            if (rCount + lCount > 5)
                return true;
            else
                return false;
        }
        #endregion

        #region движение поля
        //--поле вправо
        private void btRight_Click(object sender, EventArgs e)
        {
            _i0--;
            FillField();

        }
        //--поле вверх
        private void btUp_Click(object sender, EventArgs e)
        {
            _j0++;
            FillField();
        }
        //--поле влево
        private void btLeft_Click(object sender, EventArgs e)
        {
            _i0++;
            FillField();
        }
        //--поле вниз
        private void btDown_Click(object sender, EventArgs e)
        {
            _j0--;
            FillField();
        }

        #endregion

        #region режимы игры
        private void rbAvP_CheckedChanged(object sender, EventArgs e)
        {
            if (rbAvP.Checked)
            {
                if (AvAThread.IsAlive)
                    AvAThread.Abort();
                bAvAStop.Visible = false;
                NewGame();
                FirstAImove();
                statusLabel.Text = $"Ход игрока";
            }

        }

        private void rbPvA_CheckedChanged(object sender, EventArgs e)
        {
            if (rbPvA.Checked)
            {
                if (AvAThread.IsAlive)
                    AvAThread.Abort();
                bAvAStop.Visible = false;
                NewGame();
            }
        }

        private void rbPvP_CheckedChanged(object sender, EventArgs e)
        {
            if (rbPvP.Checked)
            {
                if (AvAThread.IsAlive)
                    AvAThread.Abort();
                bAvAStop.Visible = false;
                NewGame();
            }
        }

        private void rbAvA_CheckedChanged(object sender, EventArgs e)
        {
            if (rbAvA.Checked)
            {
               NewGame();
            }

        }
        //--останавливает поток
        private void AvAStop_Click(object sender, EventArgs e)
        {
            AvAThread.Abort();
            bAvAStop.Visible = false;
            btNewGame.Visible = true;
        }
        #endregion

    }
}

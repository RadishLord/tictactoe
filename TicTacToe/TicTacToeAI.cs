﻿using System;
using System.Collections.Generic;
using System.Deployment.Application;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTacToe
{
    /*
     * класс TicTacToeAI - анализирует массив ходов (x,Y,Symbol) 
     * и возвращает свой ход (х,Y)
    */
    class TicTacToeAi
    {
        List<MoveList> _attackCombList;
        List<MoveList> _defenseCombList;

        public RichTextBox Log;



        public void Move(MoveList moveList, ref int x, ref int y, int mainCymbol)
        {
            //--обнуление данных
            _attackCombList = new List<MoveList>();
            _defenseCombList = new List<MoveList>();

            Log.Invoke((MethodInvoker)delegate ()
            {
                Log.Clear();
            });
           

            //--основной алгоритм
            //--планирование атаки
            CombinationSearch(ref _attackCombList, moveList, mainCymbol);                   //--поиск комбиаций
            Prioritization(_attackCombList);                   //--анализ комбинаций - расстановка приоритетов 
            //--планирование защиты
            CombinationSearch(ref _defenseCombList, moveList, InvertSymbol(mainCymbol));    //--поиск комбиаций
             Prioritization(_defenseCombList);                  //--анализ комбинаций - расстановка приоритетов

            //------вывод в лог----
                    LogIt($"AttackCount {_attackCombList.Count}");
                    comboListLOG(_attackCombList, "\nattack:");
                    LogIt($"DefenceCount{_defenseCombList.Count}");
                    comboListLOG(_defenseCombList, "\ndefense:");
            //---------------------

            int attackTarget = 200, defenceTarget = 200;

            var attackCombCh = ChoiceCombination(ref _attackCombList, ref attackTarget);        //--выбор комбинации атаки
            var defenseCombCh = ChoiceCombination(ref _defenseCombList, ref defenceTarget);     //--выбор комбинации защиты
                  
            if (attackCombCh == -1 && defenseCombCh == -1)                                                         //--если комбинаций нет
            {
                NoCombinationChoice(ref x, ref y, moveList);
            }
            else
            {
                //--сравневаем что предпочтительней атака или защита
                //--атака предпочтительней
                LogIt($"Приоритет атаки:{attackTarget} \nПриоритет защиты:{defenceTarget}");
                if (defenceTarget > 6 && attackTarget < 100) defenceTarget += 4;   //--смещаем акцент в сторону атаки
                if (attackTarget <= defenceTarget)
                {
                    LogIt($"Бот атакует - {attackCombCh}");
                    Choice(_attackCombList, attackCombCh, ref x, ref y, mainCymbol);
                }
                else
                {
                    LogIt($"Бот защищается - {defenseCombCh}");
                    Choice(_defenseCombList, defenseCombCh, ref x, ref y, InvertSymbol(mainCymbol));
                }

            }
        }


        #region поиск комбинаций
        //--поиск комбинаций
        private void CombinationSearch(ref List<MoveList> comboList, MoveList moveList, int mainCymbol)
        {
            //--перебирает все знаки mainCymbol и ищет комбинации
            foreach (var v in moveList.List.Where(v => v.Symbol == mainCymbol))
            {
                comboList.Add(AnalysisHorizonLine(v.X, v.Y, moveList, mainCymbol));
                comboList.Add(AnalysisVerticalLine(v.X, v.Y, moveList, mainCymbol));
                comboList.Add(AnalysisDiagonalA(v.X, v.Y, moveList, mainCymbol));
                comboList.Add(AnalysisDiagonalB(v.X, v.Y, moveList, mainCymbol));
            }
            CutInvalidComb(ref comboList, mainCymbol);
           // LogIt($"1 {_attackCombList.Count}");
        }

        private void CutInvalidComb(ref List<MoveList> comboList, int Cymb)
        {
            // LogIt($"-1 {_attackCombList.Count}");

            //--удаление комбинаций где меньше 2 символов и без пустых мест 
            comboList = comboList.Where(s => s.SymbCount(0) != 0)
                .Where(s => s.SymbCount(Cymb) >= 2)
                .Distinct().ToList();
        }

        

        MoveList AnalysisHorizonLine(int x, int y, MoveList moveList, int mainCymbol)
        {
            int enemySymbol = InvertSymbol(mainCymbol);
            MoveList comb = new MoveList();
            //--откат до 3-х пустых или креста
            int xx = x, yy = y;
            int emptyCount = 0;
            int c = -1;
            while (emptyCount != 3 && c != enemySymbol)
            {
                c = moveList.GetSymbol(--xx, yy);
                if (c == 0)
                    emptyCount++;
            }
            //xx = x; yy = y;
            //--движемся вправо до 3-х пустых или креста и записываем в comb
            emptyCount = 0;
            c = -1;
            while (emptyCount != 3 && c != enemySymbol)
            {
                c = moveList.GetSymbol(++xx, yy);
                comb.Add(xx, yy, c);
                if (c == 0)
                {
                    emptyCount++;
                }
                else if (c == mainCymbol)
                {
                    emptyCount = 0;
                }
            }
            CrossCut(ref comb, mainCymbol); //--обрезака комбинации
            return comb;
        }
        MoveList AnalysisVerticalLine(int x, int y, MoveList moveList, int mainCymbol)
        {
            int enemySymbol = InvertSymbol(mainCymbol);
            MoveList comb = new MoveList();
            //--откат до 3-х пустых или креста
            int xx = x, yy = y;
            int emptyCount = 0;
            while (true)
            {
                int c = moveList.GetSymbol(xx, --yy);
                if (c == 0)
                    emptyCount++;
                else if (c == enemySymbol)
                    break;
                if (emptyCount == 3)
                    break;
            }
            //xx = x; yy = y;
            //--движемся вправо до 3-х пустых или креста и записываем в comb
            emptyCount = 0;
            while (true)
            {
                int c = moveList.GetSymbol(xx, ++yy);
                comb.Add(xx, yy, c);
                if (c == 0)
                    emptyCount++;
                else if (c == mainCymbol)
                {
                    emptyCount = 0;
                }
                else if (c == enemySymbol)
                    break;
                if (emptyCount == 3)
                    break;
            }
            CrossCut(ref comb, mainCymbol); //--обрезака комбинации
            return comb;
        }
        MoveList AnalysisDiagonalA(int x, int y, MoveList moveList, int mainCymbol)
        {
            int enemySymbol = InvertSymbol(mainCymbol);
            MoveList comb = new MoveList();
            //--откат до 3-х пустых или креста
            int xx = x, yy = y;
            int emptyCount = 0;
            while (true)
            {
                int c = moveList.GetSymbol(--xx, --yy);
                if (c == 0)
                    emptyCount++;
                else if (c == enemySymbol)
                    break;
                if (emptyCount == 3)
                    break;
            }
            //xx = x; yy = y;
            //--движемся вправо до 3-х пустых или креста и записываем в comb
            emptyCount = 0;
            while (true)
            {
                int c = moveList.GetSymbol(++xx, ++yy);
                comb.Add(xx, yy, c);
                if (c == 0)
                    emptyCount++;
                else if (c == mainCymbol)
                {
                    emptyCount = 0;
                }
                else if (c == enemySymbol)
                    break;
                if (emptyCount == 3)
                    break;
            }
            CrossCut(ref comb, mainCymbol); //--обрезака комбинации
            return comb;
        }
        MoveList AnalysisDiagonalB(int x, int y, MoveList moveList, int mainCymbol)
        {
            int enemySymbol = InvertSymbol(mainCymbol);
            MoveList comb = new MoveList();
            //--откат до 3-х пустых или креста
            int xx = x, yy = y;
            int emptyCount = 0;
            while (true)
            {
                int c = moveList.GetSymbol(--xx, ++yy);
                if (c == 0)
                    emptyCount++;
                else if (c == enemySymbol)
                    break;
                if (emptyCount == 3)
                    break;
            }
            //xx = x; yy = y;
            //--движемся вправо до 3-х пустых или креста и записываем в comb
            emptyCount = 0;
            while (true)
            {
                int c = moveList.GetSymbol(++xx, --yy);
                comb.Add(xx, yy, c);
                if (c == 0)
                    emptyCount++;
                else if (c == mainCymbol)
                {
                    emptyCount = 0;
                }
                else if (c == enemySymbol)
                    break;
                if (emptyCount == 3)
                    break;
            }
            CrossCut(ref comb, mainCymbol); //--обрезака комбинации
            return comb;
        }

        void CrossCut(ref MoveList comb, int mainCymbol)
        {
            int enemySymbol = InvertSymbol(mainCymbol);
            //--вырезает кресты
            for (int i = 0; i < comb.List.Count; i++)
            {
                if (comb.List[i].Symbol == enemySymbol)
                    comb.List.RemoveAt(i);
            }
            //--считает кол-во нулей
            int symbCount = comb.List.Count(v => v.Symbol == mainCymbol);
            if (symbCount < 2)
                return;


            //--когда 4 нуля
            int emptyCountBefore = 0;
            for (int i = 0; ; i++)
            {
                if (comb.List[i].Symbol != 0)
                    break;
                else
                    emptyCountBefore++;
            }

            int emptyCountAfter = 0;
            for (int i = comb.List.Count - 1; ; i--)
            {
                if (comb.List[i].Symbol != 0)
                    break;
                else
                    emptyCountAfter++;
            }

            //--удаление с начала списка
            for (int i = 0; i < emptyCountBefore - (5 - symbCount); i++)
            {
                comb.List.RemoveAt(i);
            }
            //--удаление с конца списка
            int n = comb.Count() - 1;   //--последний элемент
            int zeroNeed = 5 - symbCount;
            for (int i = n; emptyCountAfter > zeroNeed; --i)
            {
                comb.List.RemoveAt(i);
                emptyCountAfter--;

            }

        }
        #endregion

        //--анализ и распределение приоритетов
        private void Prioritization(List<MoveList> combList)
        {
            //potentialList = new List<int>(_attackCombList.Count);
            foreach (var t in combList)
            {
                int pot = StrategyPattern.Potential(t);

                t.Potential = pot;
            }
        }

        //--Сортировка комбинаций по потенциалам и возврат самой потенциальной 
        private int ChoiceCombination(ref List<MoveList> combList, ref int w)
        {
            combList = combList.OrderBy(s => s.Potential).ToList();

            if (combList.Count == 0 || combList[0].Potential == 100)
                return -1;
            w = combList[0].Potential;
            return 0;
        }

        //--выбор хода в условиях отсутствия комбинации
        private void NoCombinationChoice(ref int x, ref int y, MoveList moveList)
        {
            if (moveList.List.Count == 0)
            {
                x = 10;
                y = 10;
                LogIt($"Первый ход! Ставлю на {x} - {y}");
                return;;
            }
            
            Random rnd = new Random();
            foreach (var v in moveList.List.Where(v => v.Symbol == 2))
            {
                int overload = 1;
                while (true)
                {

                    x = v.X + rnd.Next(-1, 1);
                    y = v.Y + rnd.Next(-1, 1);

                    if (moveList.GetSymbol(x, y) == 0)
                    {
                        LogIt($"Комбинаций не нашёл, Нашёл ноль {v.X} - {v.Y} - ставлю рядом");
                        return;
                    }
                    if (overload > 8)
                        break;
                    overload++;
                }
            }
            //--если первый символ

            while (true)
            {
                if (moveList.GetSymbol(x, y) == 0)
                {
                    LogIt($"Комбинаций не нашёл, Ставлю рандомно на {x} - {y}");
                    return;
                }
                y += rnd.Next(-1, 1);
                x += rnd.Next(-1, 1);
            }

        }

        //--выбор места внутри выбранной комбмнации
        private void Choice(List<MoveList> comboList, int combCh, ref int x, ref int y, int symb)
        {
            try
            {
                // 0022200
                MoveList ChComb = comboList[combCh];
                combLog(ChComb, "\nВыбранная комбинация: ");
                //--если есть внутри - ставит внутрь или после
                int count = ChComb.List.Count;
                Predicate<Move> isNotZero = d => d.Symbol > 0;

                var firstSymbIndex = ChComb.List.FindIndex(isNotZero);
                var lastSymbIndex = ChComb.List.FindLastIndex(isNotZero);

                for (int i = firstSymbIndex; i < lastSymbIndex; i++)
                {
                    if (ChComb.List[i].Symbol == 0)
                    {
                        x = ChComb.List[i].X;
                        y = ChComb.List[i].Y;
                        LogIt($"Нашел ноль в середине комбинации. ({x};{y})");
                        return;
                    }
                }

                int zeroBefore = ZeroCount(0, firstSymbIndex, ChComb.List);
                int zeroAfter = ZeroCount(lastSymbIndex, count, ChComb.List);

                if (zeroBefore >= zeroAfter)
                {                
                    x = ChComb.List[firstSymbIndex-1].X;
                    y = ChComb.List[firstSymbIndex-1].Y;
                    LogIt($"Ставлю перед комбинацией. ({x};{y})");
                }
                else
                {                    
                    x = ChComb.List[lastSymbIndex + 1].X;
                    y = ChComb.List[lastSymbIndex + 1].Y;
                    LogIt($"Ставлю после комбинациии. ({x};{y})");
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        //--возвращает количество нулей в комбинации
        private int ZeroCount(int a, int b, List<Move> lm)
        {      
            List<Move> currList = new List<Move>();
            for (int i = a; i < b; i++)
            {
                currList.Add(lm[i]);
            }
            return currList.Count(s => s.Symbol == 0);
        }

        //--инвертирует символ
        private int InvertSymbol(int symbol)
        {
            return symbol == 2 ? 1 : 2;
        }



        #region вывод в лог
        //--вывод в лог комбо-листа
        private void combLog(MoveList comb, string str)
        {
            Log.Invoke((MethodInvoker)delegate ()
            {
                    Log.AppendText($"\n {str}");
                for (int i = 0; i < comb.Count(); i++)
                {
                    Log.AppendText(comb.List[i].Symbol.ToString());
                }
            });
        }
        //--вывод в лог комбо-листа
        private void comboListLOG(List<MoveList> comboList, string str)
        {
            Log.Invoke((MethodInvoker)delegate ()
            {
                Log.AppendText($"{str}");
                for (int i = 0; i < comboList.Count; i++)
                {
                    combLog(comboList[i], "");
                    Log.AppendText($" --- {comboList[i].Potential}");
                }
            });
           
        }
        //--вывод в лог
        private void LogIt(string str)
        {
            Log.Invoke((MethodInvoker)delegate ()
            {
                Log.AppendText($"\n{str}");
            });
        }
        #endregion

    }
}

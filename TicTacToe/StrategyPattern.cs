﻿using System.Linq;
using System.Windows.Forms;

namespace TicTacToe
{
    /*
     * StrategyPattern - статический класс, принимающий комбинацию,
     * и возвращающий потенциал хода
     */
    public static class StrategyPattern
    {
        //--Оценивает потенциал комбинации и возвращает целое. Чем меньше целое - тем выше потенциал
        public static int Potential(MoveList combination)
        {
            int[] comb = ConvertToArray(combination);
            if (MtxCompare(comb, new int[] { 0, 2, 2, 2, 2, 0 })) return 1; // Открытая четверка. Один ход до победы, 100% победа (соперник не может закрыть одним ходом)
            if (MtxCompare(comb, new int[] { 0, 2, 0, 2, 2, 0 })) return 1; // Открытая четверка. Один ход до победы, 100% победа (соперник не может закрыть одним ходом)
            if (MtxCompare(comb, new int[] { 0, 2, 2, 0, 2, 0 })) return 1; // Открытая четверка. Один ход до победы, 100% победа (соперник не может закрыть одним ходом)
            if (MtxCompare(comb, new int[] { 0, 2, 2, 2, 0, 2, 0 })) return 1; // Открытая четверка. Один ход до победы, 100% победа (соперник не может закрыть одним ходом)
            if (MtxCompare(comb, new int[] { 0, 2, 2, 0, 2, 2, 0 })) return 1; // Открытая четверка. Один ход до победы, 100% победа (соперник не может закрыть одним ходом)


            

            if (MtxCompare(comb, new int[] { 0, 2, 2, 2, 2 })) return 1;            // Полузакрытая четверка. Один ход до победы, но соперник может заблокировать
            if (MtxCompare(comb, new int[] { 2, 2, 2, 2, 0 })) return 1;            // Полузакрытая четверка. Один ход до победы, но соперник может заблокировать

            if (MtxCompare(comb, new int[] { 0, 2, 0, 2, 2, 2, 0 })) return 2;            // Четверка с брешью. Один ход до победы, но соперник может заблокировать
            if (MtxCompare(comb, new int[] { 0, 2, 0, 2, 2, 2, 0 })) return 2;            // Четверка с брешью. Один ход до победы, но соперник может заблокировать
            if (MtxCompare(comb, new int[] { 0, 2, 0, 2, 2, 2, 0 })) return 2;            // Четверка с брешью. Один ход до победы, но соперник может заблокировать

            if (MtxCompare(comb, new int[] { 0, 0, 2, 0, 2, 2, 0, 0 })) return 2;            // Четверка с брешью. Один ход до победы, но соперник может заблокировать
            if (MtxCompare(comb, new int[] { 0, 0, 2, 2, 0, 2, 0, 0 })) return 2;            // Четверка с брешью. Один ход до победы, но соперник может заблокировать

            if (MtxCompare(comb, new int[] { 2, 0, 0, 2, 2, 2, 0 })) return 2;
            if (MtxCompare(comb, new int[] { 0, 2, 0, 2, 2, 2 })) return 2;            // Четверка с брешью. Один ход до победы, но соперник может заблокировать
            if (MtxCompare(comb, new int[] { 0, 2, 2, 0, 2, 2 })) return 2;            // Четверка с брешью. Один ход до победы, но соперник может заблокировать
            if (MtxCompare(comb, new int[] { 0, 2, 2, 2, 0, 2})) return 2;            // Четверка с брешью. Один ход до победы, но соперник может заблокировать

            if (MtxCompare(comb, new int[] { 2, 0, 2, 2, 2, 0 })) return 2;            // Четверка с брешью. Один ход до победы, но соперник может заблокировать
            if (MtxCompare(comb, new int[] { 2, 2, 0, 2, 2, 0 })) return 2;            // Четверка с брешью. Один ход до победы, но соперник может заблокировать
            if (MtxCompare(comb, new int[] { 2, 2, 2, 0, 2, 0 })) return 2;            // Четверка с брешью. Один ход до победы, но соперник может заблокировать

            if (MtxCompare(comb, new int[] { 0, 0, 2, 2, 2, 0, 0 })) return 3;      // Открытая тройка (как 2 полузакрытых)

            if (MtxCompare(comb, new int[] { 0, 0, 2, 2, 2, 0 })) return 4;      // Открытая тройка (как 2 полузакрытых)
            if (MtxCompare(comb, new int[] { 0, 2, 2, 2, 0, 0 })) return 4;      // Открытая тройка (как 2 полузакрытых)

            if (MtxCompare(comb, new int[] { 2, 2, 0, 2, 0, 2, 0 })) return 5;

            if (MtxCompare(comb, new int[] { 0, 2, 2, 2, 0 })) return 5;      // Открытая тройка (как 2 полузакрытых)
            if (MtxCompare(comb, new int[] { 2, 0, 2, 2, 0 })) return 5;      // Открытая тройка (как 2 полузакрытых)
            if (MtxCompare(comb, new int[] { 2, 2, 0, 2, 0 })) return 5;      // Открытая тройка (как 2 полузакрытых)

            if (MtxCompare(comb, new int[] { 0, 0, 2, 2, 2 })) return 5;      // Открытая тройка (как 2 полузакрытых)
            if (MtxCompare(comb, new int[] { 0, 2, 0, 2, 2 })) return 5;      // Открытая тройка (как 2 полузакрытых)
            if (MtxCompare(comb, new int[] { 0, 2, 2, 0, 2 })) return 5;      // Открытая тройка (как 2 полузакрытых)

            if (MtxCompare(comb, new int[] { 2, 2, 2, 0, 0 })) return 6;      // Открытая тройка (как 2 полузакрытых)
            if (MtxCompare(comb, new int[] { 0, 0, 2, 2, 2 })) return 6;      // Открытая тройка (как 2 полузакрытых)

            if (MtxCompare(comb, new int[] { 0, 0, 0, 2, 2, 0, 0, 0 })) return 7;      // Открытая двойка (как 2 полузакрытых)

            if (MtxCompare(comb, new int[] { 0, 0, 2, 2, 0, 0, 0 })) return 8;      // Открытая тройка (как 2 полузакрытых)
            if (MtxCompare(comb, new int[] { 0, 0, 0, 2, 2, 0, 0 })) return 8;      // Открытая двойка (как 2 полузакрытых)

            if (MtxCompare(comb, new int[] { 0, 2, 2, 0, 0 })) return 9;      // Открытая тройка (как 2 полузакрытых)
            if (MtxCompare(comb, new int[] { 0, 0, 2, 2, 0 })) return 9;      // Открытая двойка (как 2 полузакрытых)

            if (MtxCompare(comb, new int[] { 2, 2, 0, 0, 0 })) return 10;      // Открытая тройка (как 2 полузакрытых)
            if (MtxCompare(comb, new int[] { 0, 0, 0, 2, 2 })) return 10;      // Открытая двойка (как 2 полузакрытых)

            //MessageBox.Show($"Не известная комбинация: {comb}");
            return 100;
        }


        public static int[] ConvertToArray(MoveList combination)
        {
            return combination.List.Select(s => s.Symbol == 0 ? 0 : 2).ToArray();
        }

        public static bool MtxCompare(int[] a, int[] b)
        {
            return a.Length == b.Length && !a.Where((t, i) => t != b[i]).Any();
        }
    }
}
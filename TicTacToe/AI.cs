﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace TicTacToe
{
    /*
     * класс AI - анализирует массив ходов (x,Y,Symbol) 
     * и возвращает свой ход (х,Y)
    */
    public class AI
    {
        public RichTextBox Log;         //--для вывода информации



        public void Move(MoveList moveList, ref int x, ref int y, int myCymbol)
        {
            
            //--Поиск всех комбинаций 

            //--Оценка потенциалов комбинации

            //--
            
        }



        #region вывод в лог
        //--вывод в лог комбо-листа
        private void combLog(MoveList comb, string str)
        {
            Log.AppendText($"\n {str}");
            for (int i = 0; i < comb.Count(); i++)
            {
                Log.AppendText(comb.List[i].Symbol.ToString());
            }
        }
        //--вывод в лог комбо-листа
        private void comboListLOG(List<MoveList> comboList, string str, List<int> potencialList)
        {
            Log.AppendText($"{str}");
            for (int i = 0; i < comboList.Count; i++)
            {
                combLog(comboList[i], "");
                if (i < potencialList.Count)
                    Log.AppendText($" --- {potencialList[i]}\n");
            }


        }
        #endregion
    }
}
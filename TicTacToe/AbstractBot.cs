﻿using System;
using TicTacToe;

namespace IBot
{
    public abstract class AbstractBot
    {
        public Field Field { get; set; }

        public CellState OpponentState { get; private set; }

        CellState _myCellState;
        public CellState State
        {
            get
            {
                return _myCellState;
            }
            set
            {
                if(value != CellState.Empty)
                {
                    _myCellState = value;
                    OpponentState = (_myCellState == CellState.Tick) ? CellState.Tack : CellState.Tick;
                }
                else
                {
                    throw new ArgumentException("Бот не может ходить пустым местом");
                }
            }
        }

        public AbstractBot(Field field, CellState state)
        {
            Field = field;
            State = state;
        }

        public abstract Cell Step();
    }
}

﻿namespace TicTacToe
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.btDown = new System.Windows.Forms.Button();
            this.btRight = new System.Windows.Forms.Button();
            this.btLeft = new System.Windows.Forms.Button();
            this.btUp = new System.Windows.Forms.Button();
            this.label0 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.btNewGame = new System.Windows.Forms.Button();
            this.rbPvP = new System.Windows.Forms.RadioButton();
            this.rbPvA = new System.Windows.Forms.RadioButton();
            this.rtbLog = new System.Windows.Forms.RichTextBox();
            this.rbAvP = new System.Windows.Forms.RadioButton();
            this.rbAvA = new System.Windows.Forms.RadioButton();
            this.bAvAStop = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox
            // 
            this.pictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox.BackColor = System.Drawing.Color.White;
            this.pictureBox.Location = new System.Drawing.Point(36, 41);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(400, 400);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox.TabIndex = 0;
            this.pictureBox.TabStop = false;
            this.pictureBox.Click += new System.EventHandler(this.pictureBox_Click_1);
            this.pictureBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseMove);
            // 
            // btDown
            // 
            this.btDown.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btDown.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btDown.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btDown.Location = new System.Drawing.Point(36, 440);
            this.btDown.Name = "btDown";
            this.btDown.Size = new System.Drawing.Size(400, 23);
            this.btDown.TabIndex = 1;
            this.btDown.Text = "V";
            this.btDown.UseVisualStyleBackColor = true;
            this.btDown.Click += new System.EventHandler(this.btDown_Click);
            // 
            // btRight
            // 
            this.btRight.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btRight.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btRight.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btRight.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btRight.Location = new System.Drawing.Point(435, 41);
            this.btRight.Name = "btRight";
            this.btRight.Size = new System.Drawing.Size(23, 400);
            this.btRight.TabIndex = 2;
            this.btRight.Text = ">";
            this.btRight.UseVisualStyleBackColor = true;
            this.btRight.Click += new System.EventHandler(this.btRight_Click);
            // 
            // btLeft
            // 
            this.btLeft.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.btLeft.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btLeft.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btLeft.Location = new System.Drawing.Point(14, 41);
            this.btLeft.Name = "btLeft";
            this.btLeft.Size = new System.Drawing.Size(23, 400);
            this.btLeft.TabIndex = 3;
            this.btLeft.Text = "<";
            this.btLeft.UseVisualStyleBackColor = true;
            this.btLeft.Click += new System.EventHandler(this.btLeft_Click);
            // 
            // btUp
            // 
            this.btUp.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btUp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btUp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btUp.Location = new System.Drawing.Point(36, 19);
            this.btUp.Name = "btUp";
            this.btUp.Size = new System.Drawing.Size(400, 23);
            this.btUp.TabIndex = 4;
            this.btUp.Text = "^";
            this.btUp.UseVisualStyleBackColor = true;
            this.btUp.Click += new System.EventHandler(this.btUp_Click);
            // 
            // label0
            // 
            this.label0.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label0.AutoSize = true;
            this.label0.Location = new System.Drawing.Point(473, 19);
            this.label0.Name = "label0";
            this.label0.Size = new System.Drawing.Size(28, 13);
            this.label0.TabIndex = 5;
            this.label0.Text = "0 - 0";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 472);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(656, 22);
            this.statusStrip1.TabIndex = 6;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // statusLabel
            // 
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // btNewGame
            // 
            this.btNewGame.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btNewGame.Location = new System.Drawing.Point(476, 140);
            this.btNewGame.Name = "btNewGame";
            this.btNewGame.Size = new System.Drawing.Size(168, 43);
            this.btNewGame.TabIndex = 7;
            this.btNewGame.Text = "Старт";
            this.btNewGame.UseVisualStyleBackColor = true;
            this.btNewGame.Visible = false;
            this.btNewGame.Click += new System.EventHandler(this.btNewGame_Click);
            // 
            // rbPvP
            // 
            this.rbPvP.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rbPvP.AutoSize = true;
            this.rbPvP.Location = new System.Drawing.Point(476, 41);
            this.rbPvP.Name = "rbPvP";
            this.rbPvP.Size = new System.Drawing.Size(118, 17);
            this.rbPvP.TabIndex = 8;
            this.rbPvP.Text = "Player 1 vs Player 2";
            this.rbPvP.UseVisualStyleBackColor = true;
            this.rbPvP.CheckedChanged += new System.EventHandler(this.rbPvP_CheckedChanged);
            // 
            // rbPvA
            // 
            this.rbPvA.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rbPvA.AutoSize = true;
            this.rbPvA.Checked = true;
            this.rbPvA.Location = new System.Drawing.Point(476, 64);
            this.rbPvA.Name = "rbPvA";
            this.rbPvA.Size = new System.Drawing.Size(81, 17);
            this.rbPvA.TabIndex = 8;
            this.rbPvA.TabStop = true;
            this.rbPvA.Text = "Player vs AI";
            this.rbPvA.UseVisualStyleBackColor = true;
            this.rbPvA.CheckedChanged += new System.EventHandler(this.rbPvA_CheckedChanged);
            // 
            // rtbLog
            // 
            this.rtbLog.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbLog.Location = new System.Drawing.Point(476, 189);
            this.rtbLog.Name = "rtbLog";
            this.rtbLog.Size = new System.Drawing.Size(168, 274);
            this.rtbLog.TabIndex = 9;
            this.rtbLog.Text = "";
            // 
            // rbAvP
            // 
            this.rbAvP.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rbAvP.AutoSize = true;
            this.rbAvP.Location = new System.Drawing.Point(476, 87);
            this.rbAvP.Name = "rbAvP";
            this.rbAvP.Size = new System.Drawing.Size(81, 17);
            this.rbAvP.TabIndex = 8;
            this.rbAvP.Text = "AI vs Player";
            this.rbAvP.UseVisualStyleBackColor = true;
            this.rbAvP.CheckedChanged += new System.EventHandler(this.rbAvP_CheckedChanged);
            // 
            // rbAvA
            // 
            this.rbAvA.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rbAvA.AutoSize = true;
            this.rbAvA.Location = new System.Drawing.Point(476, 110);
            this.rbAvA.Name = "rbAvA";
            this.rbAvA.Size = new System.Drawing.Size(62, 17);
            this.rbAvA.TabIndex = 8;
            this.rbAvA.Text = "AI vs AI";
            this.rbAvA.UseVisualStyleBackColor = true;
            this.rbAvA.CheckedChanged += new System.EventHandler(this.rbAvA_CheckedChanged);
            // 
            // bAvAStop
            // 
            this.bAvAStop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bAvAStop.Location = new System.Drawing.Point(545, 106);
            this.bAvAStop.Name = "bAvAStop";
            this.bAvAStop.Size = new System.Drawing.Size(99, 24);
            this.bAvAStop.TabIndex = 10;
            this.bAvAStop.Text = "Стоп";
            this.bAvAStop.UseVisualStyleBackColor = true;
            this.bAvAStop.Visible = false;
            this.bAvAStop.Click += new System.EventHandler(this.AvAStop_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(656, 494);
            this.Controls.Add(this.bAvAStop);
            this.Controls.Add(this.rtbLog);
            this.Controls.Add(this.rbPvA);
            this.Controls.Add(this.rbAvA);
            this.Controls.Add(this.rbAvP);
            this.Controls.Add(this.rbPvP);
            this.Controls.Add(this.btNewGame);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.label0);
            this.Controls.Add(this.btUp);
            this.Controls.Add(this.btLeft);
            this.Controls.Add(this.btRight);
            this.Controls.Add(this.btDown);
            this.Controls.Add(this.pictureBox);
            this.MinimumSize = new System.Drawing.Size(672, 532);
            this.Name = "Form1";
            this.Text = "Tic-tac-toe";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Button btDown;
        private System.Windows.Forms.Button btRight;
        private System.Windows.Forms.Button btLeft;
        private System.Windows.Forms.Button btUp;
        private System.Windows.Forms.Label label0;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel statusLabel;
        private System.Windows.Forms.Button btNewGame;
        private System.Windows.Forms.RadioButton rbPvP;
        private System.Windows.Forms.RadioButton rbPvA;
        private System.Windows.Forms.RichTextBox rtbLog;
        private System.Windows.Forms.RadioButton rbAvP;
        private System.Windows.Forms.RadioButton rbAvA;
        private System.Windows.Forms.Button bAvAStop;
    }
}

